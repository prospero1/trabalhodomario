                                             DISQUETE (*título principal*)

                                                INTRODUÇÃO (*subtítulo*)

O trabalho discorre acerca do disquete, dividindo-se na sua história, arquitetura e funcionamento.

                                                HISTÓRIA (*subtítulo*)

Criado em 1967, por um equipe liderada por Alan Shugart na IBM, onde a mesma desejava uma maneira mais barata e facil de armazenar dados, o disquete iniciou sua carreira com incriveis (para a época, se for levado em consideração seu tamanho) 80kb de espaço para armazenamento em apenas 8 polegadas (aproximadamente 20,32cm), muito parecido com um disco de vinil já que ainda não possuia proteção(*o primeiro disquete (lembrando que esse não possuia capa de proteção) ao lado de um disco de vinil para comparar*). O disquete só recebeu esse nome quando finalmente ficou da forma que conhecemos hoje, ou seja, com a capa plastica de proteção. Isso ocorreu, pois, os dados do dispositivo eram facilmente danificados com poeira e sujeiras do ambiente, o que só durou até 1976, com o disquete de 5,25 polegadas (13,335cm), que recebeu um case de plastico duro (*foto deste*). 
O disquete teve seu inicio no mercado em 1971, e nessa época era comumente usado para armazenar dados e não rodar os dados no próprio disquete (semelhante a um pendrive), pois os drives leitores eram consideravelmente mais caros (com o passar do tempo eles ficaram baratos).
A partir da década de 80 os computadores suportavam tanto o disquete de 5,25 pol quanto o 3,5 pol, fato que só mudou na década de 90, epóca em que a grande maioria dos computadores passaram aceitar (por padrão) apenas o disquete de 3,5 pol. Ainda na década de 90, muitas empresas entraram no ramo de produção de disquete para tentar uma fatia no mercado, dentre elas a que mais merece destaque aqui é a Sony, com o seu disquete de 3,5 pol no patamar mais alto de popularidade (*foto desse*).
Oficialmente o apice de capacidade armazenamento de um disquete é de 1,44mb, que nos dias de hoje não comporta nem mesmo uma musica com mais de 5 minutos em formato de MP3. As motivações para o abandono dos disquetes foram relativamente simples, custo e tempo. Estas unidades de armazenamento até podiam guardar programas em seu interior, todavia, os programas sofrem com o passar do tempo, melhorias e introdução de novos recursos, além disso novos softwares são inventados e concebidos a todo momento, e tudo isso requer espaço, um tal que o disquete não podia oferecer em um só dispositivo, seriam necessarios muitos outros disquetes, e no final das contas não fica barato, tudo isso aliado ao risco de se perder todo o processo de gravação caso alguma das midias se corromperem na hora de gravar, fato que obrigaria o usuario reiniciar todo o processo. Com o surgimento de midias de armazenamento maiores e mais baratas, como o CD-ROM, o disquete pouco a pouco perdeu espaço até se tornar obsoleto e cair em desuso pela maioria da população.
É legal ressaltar que o primeiro virus de PC foi trasmitido por um Disquete. Chamado de Elk Cloner ele foi criado por um adoslecente chamado Rich Skrenta. Rich fez um código de 400 linhas que funcionava apenas em S.O´s da Apple DOS 3.3. Quando o disquete infectado era aberto nesse sistema o computador exibia a seguinte mensagem (traduzida do inglês para o português) (*Se conseguir uma foto desse virus em exibição vai ser da hora*):

Elk Cloner: O programa com personalidade
Ele vai ficar em todos os seus discos
Ele vai se infiltrar em seus chips
Sim, é o Cloner!
Ele vai grudar em você como cola
Ele vai modificar a RAM também
Envie o Cloner!

Qualquer um que inserisse um disco sem fazer uma reinicialização limpa era infectado, e no final das contas era só isso mesmo, apenas uma mensagem para assustar os usuarios.

                                                ARQUITETURA (*subtítulo*)

Os disquetes são divididos em linhas e pistas repartidas, com intervalos numerados. Cada intervalo é denominado de Cilindro, o qual é dividido em partes iguais, chamadas de Setor, cuja capacidade é de 512 Bytes e é a menor parte do disco que pode ser lida e reconhecida pelo leitor. O disco magnético por sua vez, pode ser dividido em duas faces, e para verificar qual a capacidade, é preciso aplicar a fórmula Quantidade de Setores X Número de Pistas X Número de Faces.
A mídia magnética de um disquete é composta de óxido de ferro, basicamente ferrugem. É aplicada uma fina camada desse material sobre um disco feito de plástico mylar, o mesmo utilizado nas antigas fitas K-7.
Assim como nos discos rígidos, os disquetes são divididos em trilhas e setores. A diferença é que, enquanto um disco rígido atual possui mais de 100.000 trilhas, um disquete de 1.44 MB possui apenas 80 trilhas. O número de setores também é muito menor, apenas 18 setores por trilha num disquete de 1.44, muito longe dos 1200 ou 1500 setores por trilha dos HDs. Cada setor possui 512 bytes, o que dá um total 720 KB por face. Nos disquetes de 1.44 são usadas as duas faces do disco, chegando aos 1.44 MB nominais.

FUNCIONAMENTO (*subtítulo*)
                                                
                                                FUNCIONAMENTO DO DISQUETE

É preciso entender o funcionamento do Disquete, e para isso serão usados exemplos e abstrações simples:
Imagine um armazem com algumas salas, e dentro das salas algumas caixas, coloquemos aqui em exemplo 2 caixas para cada sala. Cada caixa possui uma etiqueta com um numero indicando a sala em que a mesma esta, aliada a um numero unico que representa essa caixa especifica e que não se repete nas outras caixas. Quando o entregador colocar um item em qualquer que seja a caixa, ele vai associar o numero da caixa com o item lá colocado e documentar. Quando alguma outra pessoa for acessar o conteudo das caixas, vai seguir essa determinada documentação para saber a sala em que deve entrar e qual caixa deve ser aberta. (*uma foto desse passo-a-passo mental é legal Fabião*)
No caso dos disquetes, as salas são setores e cada setor do disco possui diversas trilhas, que são as caixas, as etiquetas equivalem ao endereço que cada arquivo (conteudo) vai receber. Nessas trilhas o computador ira gravar os arquivos. (*foto dessa estrutura com as devidas setas indicando os nomes*)

Agora, tentemos entender como é esse processo de gravação e leitura. A estrutura base do disquete é oxido de ferro, e seu funcionamento é baseado em um processo magnético. Esse óxido de ferro forma diversos microímas em seu interior. Assim, cada bit que gravamos num disquete vai gerar uma pequeníssima região na trilha que produz um determinado campo magnético. Cada bit é então representado por um imã microscópico ou elementar que têm determinada orientação. A orientação não depende somente do bit, se é zero ou um, mas também do processo usado na codificação dos dados que pode variar bastante. E na hora de ler é bem simples, como cada espaço dessa trilha esta magnetizado de certa forma, quando o leitor passa pela região da trilha que está magnetizada, essa gera um impulso elétrico de determinada polaridade (norte ou sul) que vai ser reconhecida pelos circuitos. Quando o disquete gira diante da cabeça de leitura, a passagem da pequena região magnetizada que corresponde ao bit gera um impulso elétrico de determinada polaridade e que pode ser reconhecido pelos circuitos. 
O disquete possui também um método de proteção de seus arquivos, é este o famoso interruptor, que quando posicionado para baixo tapando o pequeno buraco na parte de cima estara liberando o botão que será pressionado ao ser inserido no drive, permitindo a gravação, e caso esse mesmo interruptor esteja para cima, o botão será tapado e não poderá ser pressionado no drive, impedindo assim a gravação. (*fotinha dessa parte*)
Tudo aquilo que se relaciona com o campo magnético - raios gamas, aparelhos elétricos, imãs, o campo magnético da Terra e etc - influenciam no magnetismo do disquete, ato que pode mudar a orientação magnética dos microímãs e assim mudar o impulso magnético que vai ser gerado na hora da leitura. O tempo de integridade média dos arquivos foram gravados no disquete e o próprio dispositivo tem em média, vida útil, de aproximadamente 5 anos (*foto das coisas que afetam um disquete*).

                                                FUNCIONAMENTO DO DRIVE

O drive de disquete é mais simples que o próprio disquete, já que seu controlador interno é apenas mêcanico, ou seja, o drive de disquete é totalmente dependente do controlador de disquete presente no Super I/O da placa mãe (parte responsavel pelo controle algumas interfaces do pc, dentre elas o disquete), via Floppy Disk Controller, ou FDC. Essa controladora é responsável pelo controle de informações, mapeamento dos setores do drive, envio de comandos, detecção de erros e etc.. Basicamente o drive de disquete possui 5 estruturas principais, são elas: 
Controlador do motor, 
conector do motor de passo, 
conector para a interface com a controladora, 
circuito integrado que controla os motores, o disco e a gravação e leitura de dados.
conector de alimentação.

A entrada e saida de dados pode ser representada pela imagem abaixo que mosta o que cada pino do leitor de disquete faz
(*Fabião, seguinte, entra nesse link https://www.hardwarecentral.net/single-post/2018/07/18/Hardware---O-Disquete, aqui você vai ver a imagem da entrada e saida, a 14ª imagem, e é uma tabela*)

A primeira coluna indica qual a posicão do pino, a segunda fala sobre o nome do pino, a terceira indica a direção do dado, se para apontada para a direita é o que é enviado do FDC para o drive, e se apontada para a esquerda, indica uma saida de dados do drive para o FDC.

  
Links / bibliografias
    https://www.estudopratico.com.br/a-origem-do-disquete/
    https://origemdascoisas.com/a-origem-da-disquete/
    https://computer.howstuffworks.com/floppy-disk-drive1.htm
    https://redelan.wordpress.com/2018/04/11/disquete/
    https://www.infoescola.com/informatica/disquete/
    https://www.youtube.com/watch?v=2xSRTAxcYTY
    https://pt.wikipedia.org/wiki/Zip_drive#targetText=O%20zip%20drive%20foi%20baseado,montado%20em%20um%20cartucho%20robusto.
    https://www.techtudo.com.br/listas/2019/06/dez-curiosidades-sobre-disquetes-que-fizeram-sucesso-nos-pcs-dos-anos-90.ghtml
    https://www.hardwarecentral.net/single-post/2018/07/18/Hardware---O-Disquete
    http://www.dsc.ufcg.edu.br/~pet/jornal/junho2014/materias/historia_da_computacao.html
    http://www.evolucaotecnologica.com.br/?p=82
    https://www.retrocomputaria.com.br/2016/09/12/retrovideoteca-como-funciona-o-disquete/
    
